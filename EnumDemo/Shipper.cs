﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumDemo
{
    public enum Shipper
    {
        USPS,
        UPS,
        FedEx
    }
}
