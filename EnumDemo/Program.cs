﻿namespace EnumDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            Console.WriteLine(Shipper.FedEx.GetHashCode());

            int[] nums = { 1, 4, 1, 3, 2, 1, 2, 8 };

            foreach(int num in nums.Reverse())
            {

                //Array.Reverse(nums);
                Console.WriteLine(num);
            }

            Array.Sort(nums);
            Console.WriteLine("--------------------------");

            foreach(int num in nums)
            {
                Console.WriteLine(num); 
            }
            Console.WriteLine("--------------------------");

            Console.WriteLine(Array.BinarySearch(nums, 4));
        }
    }
}