﻿namespace PassByRefAndValue
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int x = 2;
            TimesTwo(x);
            Console.WriteLine("From Main: " + x);

            int[] arr = { 2, 3, 4 };
            AllTimesTwo(arr);
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("From Main: " + arr[i]);

            }
            int val = 3;
            Example(x: val, val: x);
        }

        static void Example(int val, int x, int z = 0)
        {
            Console.WriteLine("val: " + val);
            Console.WriteLine("x: " + x);
            Console.WriteLine("z: " + z);

        }

        static void TimesTwo(int x)
        {
            x = x * 2;
            Console.WriteLine("From TimesTwo: " + x);
        }

        static void AllTimesTwo(int[] arr)
        {
            for(int i = 0; i < arr.Length; i++)
            {
                arr[i] = 2 * arr[i];
                Console.WriteLine("From AllTimesTwo: " + arr[i]);

            }

        }
    }
}