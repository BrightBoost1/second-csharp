﻿namespace ExceptionHandlingExample
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int age = GetAge();
            Printer.PrintInt(age);
        }

        static int GetAge()
        {
            Console.WriteLine("Please enter a valid age:");
            string str_age = Console.ReadLine();
            int int_age = -1;
            try
            {
                int_age = Convert.ToInt32(str_age);
            }
            catch (FormatException e)
            {
                Console.WriteLine("Not a valid age. Exception: " + e);
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("Cleaning up resources");
            }

            return int_age;
        }
    }
}