﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandlingExample
{
    internal class Printer
    {
        public static void PrintInt(int message) 
        {
            Console.WriteLine("The value: " + message);
        }
    }
}
