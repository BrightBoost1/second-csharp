﻿namespace MajorCodesApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] majorCodes = { "BIOL", "CSCI", "ENG", "GEO", "HIST", "MATH", "PSCI" };
            // create an array with a cell for each value of majorCodes
            int[] countOfMajors = new int[majorCodes.Length];
            // initialize the array elements to 0
            Array.Clear(countOfMajors, 0, countOfMajors.Length);
            // Process input
            string repeat;
            
            {
                // Read student major data from the user
                Console.Write("Enter the student's major code: ");
                string inputCode = Console.ReadLine();
                // Find the major and add one to the number of majors
                int foundLocation = Array.IndexOf(majorCodes, inputCode); 
                if (foundLocation == -1)
                {
                    Console.WriteLine($"{inputCode} is not a valid major");
                }
                else
                {
                    countOfMajors[foundLocation]++;
                }
                // Ask the user if there is more data
                Console.Write("Do you want to enter another? ");
                repeat = Console.ReadLine().ToUpper();
            } while (repeat == "YES");
            // Display the final counts for each major
            for(int i = 0; i < majorCodes.Length; i++) { 
                Console.WriteLine($"There are {countOfMajors[i]} {majorCodes[i]} majors");
            }
        }
    }
    
}