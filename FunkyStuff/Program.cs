﻿using Microsoft.CSharp.RuntimeBinder;
using System.Dynamic;

namespace FunkyStuff
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            string ownerAnswer = "YES";
            dynamic animal;
            animal = new ExpandoObject();
            animal.Breed = "Golden Retriever"; 
            animal.Adopted = 2017;
            animal.Name = "Happy";
            if (ownerAnswer == "YES")
            {
               animal.AllowedOnCouch = true;
            }
            Console.WriteLine(
            $"{animal.Name} was adopted in {animal.Adopted}");

            // this should probably be dealt with differently
            try
            {
                if (animal.AllowedOnCouch == true )
                {
                    Console.WriteLine("It's okay to be on the couch");
                }
            }
            catch(RuntimeBinderException e)
            {
                Console.WriteLine ("Dynamic problem");
            }
           
        }
    }
}