﻿namespace BuiltInStuff
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string s1 = "a";
            string s2 = "b";
            Console.WriteLine(string.Compare(s1, s2));

            Console.WriteLine("Enter datetime:");
            string str = Console.ReadLine();
            DateTime dateTime = Convert.ToDateTime(str);
            Console.WriteLine($"Date time is {dateTime}");
            
            dateTime = dateTime.AddDays(7);
            Console.WriteLine($"Date time is {dateTime}");

            int[][] arr = new int[][] { 
                new int[] { 1, 2, 3, 7}, 
                new int[] { 5, 6, 7, 8, 9 } 
            };

        }
    }
}