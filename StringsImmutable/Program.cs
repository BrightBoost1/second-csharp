﻿namespace StringsImmutable
{
    class Person
    {
        public Person(String name) 
        { 
            Name = name;
        }
        public string Name { get; set; }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            string s = "hi   ";
            string s1 = "ha   ";
            Person p = new Person(s1);
            ChangeNamePersonNoRef(p);
            Console.WriteLine(p.Name);
            TrimStringNoRef(s1);
            TrimStringRef(ref s);
            Console.WriteLine(s1 + ".");
            Console.WriteLine(s + ".");
        }

        static void TrimStringNoRef(string s)
        {
            s = s.Trim();
        }

        static void TrimStringRef(ref string s)
        {
            s = s.Trim();
        }

        static void ChangeNamePersonNoRef(Person p)
        {
            p.Name = "Maaike";
        }

    }
}