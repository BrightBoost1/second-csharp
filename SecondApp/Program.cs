﻿namespace SecondApp
{
    internal class Program
    {
        static void Main(string[] args)
        {

            DateTime dt = DateTime.Today;
            int dayNum = (int)dt.DayOfWeek;
            string dayName = "";
            switch (dayNum)
            {
                case 0:
                case 6:
                    dayName = "Weekend";
                    break;
                case >= 1 and <= 5:
                    dayName = "Weekday";
                    break;
                default:
                    dayName = "Unreal day";
                    break;
            }

            // not a use case for ternirary statement
            int w = (dayNum == 1) ? 1 : dayNum == 2 ? 2 : dayNum == 3 ? 3: dayNum == 4 ? 4 : 5;


            int num1 = 8, num2 = 3;
            double answer = (float)num1 / num2;
            Console.WriteLine("Answer = {0:P}", answer);
         
            decimal price = 12728.49M;
            Console.WriteLine("Price = {0:C}", price);

            decimal sumOfTestScores = 43.1M;
            decimal numOfTestScores = 5;
            decimal average = sumOfTestScores /  numOfTestScores;
            Console.WriteLine(average);
            Console.ReadLine();


        }
    }
}